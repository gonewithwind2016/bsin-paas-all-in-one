package me.flyray.bsin.utils;

import java.util.Map;

import me.flyray.bsin.constants.ResponseCode;
import me.flyray.bsin.exception.BusinessException;

public class BsinPageUtil {

    public static void pageNotNull(Pagination pagination){
        Boolean flag = EmptyChecker.isEmpty(pagination);
        if (flag){
            throw new BusinessException(ResponseCode.PAGE_NUM_ISNULL);
        }
        Integer pageNum = pagination.getPageNum();
        Integer pageSize = pagination.getPageSize();
        if (pageNum == null || pageSize == null){
            throw new BusinessException(ResponseCode.PAGE_NUM_ISNULL);
        }
    }
}
