package me.flyray.bsin.server.mapper;


import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

import java.util.List;

import me.flyray.bsin.server.domain.TenantWxPlatformUserTag;

/**
* @author bolei
* @description 针对表【ai_tenant_wxmp_user_tag】的数据库操作Mapper
* @createDate 2023-04-28 12:46:58
* @Entity generator.domain.AiTenantWxmpUserTag
*/

@Mapper
@Repository
public interface AiTenantWxPlatformUserTagMapper {

    List<TenantWxPlatformUserTag> selectByOpenId(String openId);

    void insert(TenantWxPlatformUserTag tenantWxmpUserTag);

}
