package me.flyray.bsin.server;

import org.flowable.engine.TaskService;
import org.flowable.engine.task.Comment;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：bolei
 * @date ：Created in 2020/9/15 10:09
 * @description：任务办理
 * @modified By：
 */

@RunWith(SpringRunner.class)
@SpringBootTest
public class BsinWorkflowEngineTaskVoHandlerTests {

    @Autowired
    private TaskService taskService;

    /**
     * 任务完成处理
     */
    @Test
    public void taskComplete(){
        String taskId = "7b05b3ee-e9cd-11ea-88c8-982cbc048a9e";
        taskService.complete(taskId);
    }

    /**
     * 任务完成处理
     * 参数：taskId(对应act_ru_task中的id_)，variables（下一次任务所需要的参数）
     * 作用：完成这一次任务，并且下一步任务需要流程变量的
     */
    @Test
    public void taskCompleteWithVar(){
        String taskId = "";
        Map<String,Object> variables = new HashMap<>();
        taskService.complete(taskId,variables);
    }

    /**
     * 任务完成处理
     * 参数：taskId(对应act_ru_task中的id_)，variables（下一次任务所需要的参数），localScope（存储范围：本任务）
     * 作用：同第二个，加上存储范围的设置
     * localScope官方解释：If true, the provided variables will be stored task-local, instead of process instance wide (which is the default for complete(String, Map)).
     * （如果为true，通过的变量将会随着本任务存亡（本任务结束，变量删除，称之为任务变量（局部）），默认为false，即为complete（String,Map） 那么这时候的变量为流程变量（全局））
     */
    @Test
    public void taskCompleteWithVars(){
        String taskId = "";
        Map<String,Object> variables = new HashMap<>();
        taskService.complete(taskId,variables,true);
    }


    /**
     * 任务签收
     * 候选组用户需要先签收后完成，
     */
    @Test
    public void taskClain(){
        String taskId = "";
        String userId  = "";
        taskService.claim(taskId,userId);
    }

    /**
     * 任务的转办
     */
    @Test
    public void turnTask(){
        String taskId = "";
        String curUserId = "";
        String acceptUserId = "";
        taskService.setOwner(taskId, curUserId);
        taskService.setAssignee(taskId,acceptUserId );
    }

    /**
     * 委派任务
     */
    @Test
    public void delegateTask(){
        String taskId = "";
        String curUserId = "";
        String acceptUserId = "";
        taskService.setOwner(taskId, curUserId);
        taskService.delegateTask(taskId,acceptUserId);
    }

    /**
     * 委派任务的处理
     */
    @Test
    public void resolveTask(){
        String taskId = "";
        Map<String,Object> variables = new HashMap<>();
        taskService.resolveTask(taskId, variables);
    }

    /**
     * 任务添加评论
     * String taskId, String processInstance, String message
     */
    @Test
    public void addComment(){
        String taskId = "";
        String processInstance = "";
        String message = "";
        taskService.addComment(taskId,processInstance,message);
    }

    /**
     * 任务审核意见查询
     */
    @Test
    public void queryComments(){
        String taskId = "";
        List<Comment> comments =  taskService.getTaskComments(taskId);
    }

}
