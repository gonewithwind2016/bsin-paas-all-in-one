package me.flyray.bsin.server.impl;

import org.kie.api.KieBase;
import org.kie.api.KieServices;
import org.kie.api.builder.KieFileSystem;
import org.kie.api.builder.model.KieBaseModel;
import org.kie.api.builder.model.KieModuleModel;
import org.kie.api.builder.model.KieSessionModel;
import org.kie.api.conf.EqualityBehaviorOption;
import org.kie.api.conf.EventProcessingOption;
import org.kie.api.io.Resource;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.conf.ClockTypeOption;
import org.kie.internal.io.ResourceFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import me.flyray.bsin.server.domain.Person;

@RestController
public class KieBuilderController {

    /**
     * 调用drools rule判断用户是否可以玩游戏
     */
    @GetMapping("kieBuilder")
    public Person canPlayGame(Person person) {

        //1.获取一个KieServices
        KieServices kieServices = KieServices.Factory.get();
        //2.创建kiemodule xml对应的class
        KieModuleModel kieModuleModel = kieServices.newKieModuleModel();
        //3.创建KieFileSystem虚拟文件系统
        KieFileSystem kieFileSystem = kieServices.newKieFileSystem();
        //4.添加具体的KieBase标签
        KieBaseModel kieBaseModel = kieModuleModel.newKieBaseModel("kbase").
                addPackage("rules");//kie fileSystem 中资源文件的文件夹
        //<KieBase></KieBase>标签添加KieSession属性
        kieBaseModel.newKieSessionModel("kiession");
        //5.添加kiemodule.xml文件到虚拟文件系统
        String kieModuleModelXml = kieModuleModel.toXML();
        kieFileSystem.writeKModuleXML(kieModuleModelXml);//kieModuleModel
        //6.把规则文件加载到虚拟文件系统
        String ruleFile = "rules/person.drl.bak";
        Resource resource = ResourceFactory.newClassPathResource(ruleFile, "UTF-8");
        //这里是把规则文件添加到虚拟系统，第一个参数是文件在虚拟系统中的路径
        kieFileSystem.write(resource);
        //7.构建所有的KieBase并把所有的KieBase添加到仓库里
        kieServices.newKieBuilder(kieFileSystem).buildAll();
        KieContainer kieContainer = kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());//创建kie容器
        //8.从容器中获取一个会话，这里和a处添加的是一个key，否则找不到 找不到任何一个会报异常
        KieSession kieSession = kieContainer.newKieSession("kiession");

        try {
            // 将输入数据提供给规则引擎
            kieSession.insert(person);
            // 激活规则引擎，如果规则匹配成功则执行规则
            kieSession.fireAllRules();
        } finally {
            kieSession.dispose();
        }
        return person;
    }

    private KieBase buildKieBase(String[] rules) {
        KieServices kieServices = KieServices.Factory.get();
        KieModuleModel kieModuleModel = kieServices.newKieModuleModel();
        KieBaseModel kieBaseModel1 = kieModuleModel.newKieBaseModel("KBase").setDefault(true)
                .setEqualsBehavior(EqualityBehaviorOption.EQUALITY)
                .setEventProcessingMode(EventProcessingOption.STREAM);
        kieBaseModel1.newKieSessionModel("KSession").setDefault(true).setType(KieSessionModel.KieSessionType.STATEFUL)
                .setClockType(ClockTypeOption.get("realtime"));
        KieFileSystem kfs = kieServices.newKieFileSystem();
        kfs.writeKModuleXML(kieModuleModel.toXML());
        for (int i = 0; i < rules.length; i++) {
            String ruleFile = rules[i];
            kfs.write(ResourceFactory.newClassPathResource(ruleFile, "UTF-8"));
        }
        kieServices.newKieBuilder(kfs).buildAll();
        KieContainer kieContainer = kieServices.newKieContainer(kieServices.getRepository().getDefaultReleaseId());
        KieBase kbase = kieContainer.getKieBase();
        return kbase;
    }
}