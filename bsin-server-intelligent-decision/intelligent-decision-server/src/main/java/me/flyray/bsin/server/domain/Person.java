package me.flyray.bsin.server.domain;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
public class Person {
    private String name;
    private Integer age;
    // 是否可以玩游戏，此字段的值，由 drools 引擎计算得出
    private Boolean canPlayGame;

}
