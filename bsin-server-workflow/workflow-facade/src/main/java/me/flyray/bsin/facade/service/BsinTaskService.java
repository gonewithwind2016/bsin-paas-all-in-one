package me.flyray.bsin.facade.service;


import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import java.util.List;
import java.util.Map;

@Path("taskService")
public interface BsinTaskService {

    /**
     * 根据用户名获取待办任务
     */
    @POST
    @Path("getTaskByUser")
    @Produces("application/json")
    public Map<String, Object> getTaskByUser(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 根据用户名获取候选任务
     */
    @POST
    @Path("getCandidateTaskByUser")
    @Produces("application/json")
    public Map<String,Object> getCandidateTaskByUser(Map<String, Object> requestMap);

    /**
     * 领取候选任务
     */
    @POST
    @Path("claimTaskCandidate")
    @Produces("application/json")
    public Map<String,Object> claimCandidateTask(Map<String, Object> requestMap);

    /**
     * 任务归还（如果任务拾取之后不想操作或者误拾取任务也可以进行归还任务）
     */
    @POST
    @Path("unClaimCandidateTask")
    @Produces("application/json")
    public Map<String,Object> unClaimCandidateTask(Map<String, Object> requestMap);

    /**
     * 任务转办
     */
    @POST
    @Path("transfer")
    @Produces("application/json")
    public Map<String, Object> transfer(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 任务委派
     */
    @POST
    @Path("delegateTask")
    @Produces("application/json")
    public Map<String, Object> delegateTask(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 任务解决
     */
    @POST
    @Path("resolve")
    @Produces("application/json")
    public Map<String, Object> resolve(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 任务完成
     */
    @POST
    @Path("complete")
    @Produces("application/json")
    public Map<String, Object> complete(Map<String, Object> requestMap) throws ClassNotFoundException;


    /**
     * 附带表单数据完成任务
     */
    @POST
    @Path("completeTaskWithForm")
    @Produces("application/json")
    public Map<String, Object> completeTaskWithForm(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 设置用户任务受理人
     */
    @POST
    @Path("complete")
    @Produces("application/json")
    public Map<String, Object>  setAssignee(Map<String, Object> requestMap) throws ClassNotFoundException;

    /**
     * 获取所有流程节点列表
     */
    @POST
    @Path("getAllNode")
    @Produces("application/json")
    Map<String,Object> getAllNode(Map<String, Object> requestMap);

    /**
     * 获取当前节点信息
     */
    @POST
    @Path("getCurrentNodeInfo")
    @Produces("application/json")
    Map<String,Object> getCurrentNodeInfo(Map<String, Object> requestMap);

    /**
     * 获取下一节点信息
     */
    @POST
    @Path("getNextNodeInfo")
    @Produces("application/json")
    Map<String,Object> getNextNodeInfo(Map<String, Object> requestMap);

    /**
     * 获取上一节点信息
     */
    @POST
    @Path("getPreviousNodeInfo")
    @Produces("application/json")
    Map<String,Object> getPreviousNodeInfo(Map<String, Object> requestMap);

    /**
     * 流程收回/驳回
     */
    @POST
    @Path("flowTackback")
    @Produces("application/json")
    Map<String,Object> flowTackback(Map<String, Object> requestMap);

    /**
     * 流程回退
     */
    @POST
    @Path("flowReturn")
    @Produces("application/json")
    Map<String,Object> flowReturn(Map<String,Object> requestMap);

    /**
     * 获取所有可回退的节点
     */
    @POST
    @Path("findReturnUserTask")
    @Produces("application/json")
    Map<String,Object> findReturnUserTask(Map<String,Object> requestMap );
}
